DROP DATABASE IF EXISTS ringette;
CREATE DATABASE ringette;
use ringette;

CREATE TABLE CodeType (codeTypeId int(3) COMMENT 'This is the primary key for code types',
  englishDescription varchar(100) NOT NULL COMMENT 'English description',
  frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
) COMMENT 'This tables holds the code types that are available for the application';

INSERT INTO CodeType (CodeTypeId, englishDescription, frenchDescription, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 'User Types', 'User Types FR', sysdate(), '', CURRENT_TIMESTAMP, ''),
(2, 'Status Types', 'Status Types FR', CURRENT_TIMESTAMP, '', CURRENT_TIMESTAMP, ''),
(3, 'Skill Types', 'Skill Types FR', CURRENT_TIMESTAMP, '', CURRENT_TIMESTAMP, '');


CREATE TABLE CodeValue (
  codeTypeId int(3) NOT NULL COMMENT 'see code_type table',
  codeValueSequence int(3) NOT NULL,
  englishDescription varchar(100) NOT NULL COMMENT 'English description',
  englishDescriptionShort varchar(20) NOT NULL COMMENT 'English abbreviation for description',
  frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
  frenchDescriptionShort varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description',
  sortOrder int(3) DEFAULT NULL COMMENT 'Sort order if applicable',
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
) COMMENT='This will hold code values for the application.';

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 1, 'General', 'General', 'GeneralFR', 'GeneralFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');
INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 2, 'Admin', 'Admin', 'Admin', 'Admin', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(2, 1, 'Active', 'Active', 'Active', 'Active', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(2, 2, 'Not Active', 'Not Active', 'Not Active', 'Not Active', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(3, 1, 'Agility', '', '', '', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 2, 'Butterfly', '', '', '', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 3, 'Shooting', '', '', '', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 4, 'Passing', '', '', '', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 5, 'Speed', '', '', '', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

-- --------------------------------------------------------


CREATE TABLE UserAccess (
  userAccessId int(3) NOT NULL,
  username varchar(100) NOT NULL COMMENT 'Unique user name for app',
  password varchar(128) NOT NULL,
  name varchar(128),
  userAccessStatusCode int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #2',
  userTypeCode int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #1',
  createdDateTime datetime DEFAULT NULL COMMENT 'When user was created.'
);

ALTER TABLE CodeType
  ADD PRIMARY KEY (codeTypeId);

ALTER TABLE CodeValue
  ADD PRIMARY KEY (codeTypeId,codeValueSequence);
--  ADD KEY codeTypeId (codeTypeId);

ALTER TABLE UserAccess
  ADD PRIMARY KEY (userAccessId),
  ADD KEY userTypeCode (userTypeCode);

ALTER TABLE CodeType
  MODIFY codeTypeId int(3) NOT NULL COMMENT 'This is the primary key for code types';

ALTER TABLE CodeValue
  MODIFY codeValueSequence int(3) NOT NULL;

ALTER TABLE UserAccess
  MODIFY userAccessId int(3) NOT NULL AUTO_INCREMENT;
-- ALTER TABLE CodeValue
--   ADD CONSTRAINT codevalue_ibfk_1 FOREIGN KEY (codeTypeId) REFERENCES CodeType (CodeTypeId);









/* create table queries */
CREATE TABLE Player (
  id int(6) NOT NULL AUTO_INCREMENT,
  firstName varchar(100) NOT NULL COMMENT 'First Name',
  lastName varchar(100) NOT NULL COMMENT 'Last Name',
  dob varchar(10) DEFAULT NULL COMMENT 'yyyy-MM-dd',
  activeTypeCode int(3) COMMENT 'Code Type 2',
  PRIMARY KEY (id)
);

CREATE TABLE PlayerSkill(
    skillId int(3) NOT NULL AUTO_INCREMENT,
    playerName varchar(200) COMMENT 'reference to player.firstName and player.lastName',
    skillTypeCode int(3) COMMENT 'Code Type 3',
    points int NOT NULL,
    skillTime decimal(3,2) NOT NULL COMMENT 'MORE ANALYSIS NEEDED HERE',
    comments varchar(500) NOT NULL COMMENT 'Comments',
	PRIMARY KEY(skillID)
 );


/* Insert queries */
INSERT INTO Player (firstName, lastName, dob, activeTypeCode) VALUES
('Nathan', 'Mackinnon', '1998-08-15', 1),
('Cale', 'Makar', '2000-02-22', 1),
('Bowen', 'Bryam', '2002-12-25', 1);



INSERT INTO PlayerSkill(playerName, skillTypeCode, points, skillTime, comments) values
('Nathan Mackinnon','1',10,0,'he was fast'),
('Cale Makar','1',9,0,'he was faster'),
('Bowen Bryam','1',8,0,'he was fastest'),
('Nathan Mackinnon','2',20,0,''),
('Cale Makar','2',20,0,'');
