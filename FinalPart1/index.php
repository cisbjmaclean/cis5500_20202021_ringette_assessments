<?php
session_start();
// Dylan Corriveau
// Date: December 6th, 2020
// CIS2288
//Final Exam 1

?>
<!doctype html>
<html>
<head>
    <title>ringette Application Home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div id="container">
    <?php
    // set up connection
    require("config.php");
    if(@!isset($_SESSION['loggedIn']) || !$_SESSION['loggedIn']){
        echo "Please log in!" . "<a href='login.php' class ='btn btn-primary btn-block'>Add Book</a>";

    }else{
        echo '<style> .loggedInCorner{display: block} .loggedInColumn{display: block}</style>';
    }
    echo "<p class='loggedInCorner'> User: " . $_SESSION['username'] .
        "<a href='logout.php' title='Log Out' class='btn btn-info' data-toggle='tooltip'>Logout</a>" ;

    //Sort type
    $sort = " order by books.title asc";

    //Display book inventory
    $query = "SELECT player.firstName, player.lastName, player.dob, codeValue.englishDescription FROM player JOIN codevalue ON codevalue.codeValueSequence = player.activeTypeCode WHERE codevalue.codeTypeId = 2";

    // Here we use our $db object created above and run the query() method. We pass it our query from above.
    $result = $mysqli->query($query);

    $num_results = $result->num_rows;
    if(isset($_GET['msg'])) {
        echo "<p>{$_GET['msg']}</p>";
    }
    echo "<p>Number of Players found: " . $num_results . "</p>";
    echo "<h2>Ringette Application</h2>";
    echo "<table class='table table-bordered table-striped'>";
    echo "<thead>";
    if ($num_results > 0) {
//  $result->fetch_all(MYSQLI_ASSOC) returns a numeric array of all the books retrieved with the query
        $players = $result->fetch_all(MYSQLI_ASSOC);
        echo "<table class='table table-bordered'><tr>";
//This dynamically retieves header names
        foreach ($players[0] as $k => $v) {

            echo "<th>" . $k . "</th>";

        }

        echo "<th>Action</th>";

        echo "</tr></thead>";
        echo "<tbody>";
//Create a new row for each book
        foreach ($players as $player) {
            echo "<tr>";
            $i = 0;

            foreach ($player as $k => $v) {

                if ($k == 'id') {
                    echo "<td>" . $v . "</td>";
                    $id = $v;
                } else {
                    echo "<td>" . $v . "</td>";
                }
                if (($i == count($player) - 1)) {
                    echo "<td class='loggedInColumn'>";
                    echo "<div class='btn-toolbar'>";
                    echo "<a href='admin/editPlayer.php?id=" . $id . "' title='Edit Record' class='btn btn-info btn-xs' data-toggle='tooltip'>Edit</a>";
                    echo "<a href='admin/deletePlayer.php?id=" . $id . "' title='Delete Record' class='btn btn-info btn-xs' data-toggle='tooltip'>Delete</a>";
                    echo "</div>";
                    echo "</td>";
                }
                $i++;
            }
            echo "</tr>";

        }

        echo "<tr><td colspan='6'>";
        echo "<a href='admin/addPlayer.php' title='View Record' class='btn btn-info' data-toggle='tooltip'>Add a New Player</a>";
        echo "</td></tr>";

        echo "</tbody>";
        echo "</table>";
    }
    // free result and disconnect
    $result->free();
    $mysqli->close();

    ?>
</div>
</body>
</html>
