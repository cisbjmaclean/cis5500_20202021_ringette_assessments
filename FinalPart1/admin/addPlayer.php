<?php
session_start();
// Dylan Corriveau
// Date: December 6th, 2020
// CIS2288
//Final Exam 1
?>
<!doctype html>
<html>
<head>
    <title>ringette Application Player - Add New Player</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="/FinalPart1/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div id="container">

    <h1>ringette Application Player - Add Player</h1>
    <?php
    if(!isset($_SESSION['loggedIn']) || !$_SESSION['loggedIn']){
        echo "Please log in!" . "<a href='/FinalPart1/login.php' class ='btn btn-primary btn-block'>Add Player</a>";
    }else{
    require_once("utilities.php");

        // create short variable names
        $firstName = test_input($_POST['firstName']);
        $lastName = test_input($_POST['lastName']);
        $dob = test_input($_POST['dob']);
        $active = test_input($_POST['active']);

        if (empty($firstName) || empty($lastName) || empty($dob) || empty($active)) {

            header("location:newPlayer.php?error=empty");
            exit();

        }
        //Create DB object
        define('DB_SERVER', 'localhost');
        define('DB_USERNAME', 'web_only_user');
        define('DB_PASSWORD', 'web_secret_password');
        define('DB_NAME', 'ringette');

        /* Attempt to connect to MySQL database */
        $mysqli = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

        $firstName = $mysqli->real_escape_string($firstName);
        $lastName = $mysqli->real_escape_string($lastName);
        $dob = $mysqli->real_escape_string($dob);
        $active = $mysqli->real_escape_string($active);

        if (mysqli_connect_errno()) {
            echo "Error: Could not connect to database.  Please try again later.";
            exit;
        }

        $query = "INSERT INTO Player (id, firstName, lastName, dob, activeTypeCode) VALUES
( 0, '$firstName', '$lastName' , '$dob' , $active)";
       // echo $query;
        $result = $mysqli->query($query);

        if ($result) {
            echo $mysqli->affected_rows . " player inserted into database. <a href='newPlayer.php'>Add another?</a>";

            //Display Player inventory
            $query = "SELECT * FROM player";
// Here we use our $mysqli object created above and run the query() method. We pass it our query from above.
            $result = $mysqli->query($query);

            $num_results = $result->num_rows;

            echo "<p>Number of players found: " . $num_results . "</p>";

            echo "<h2>Ringette Player list</h2>";
            echo "<table class='table table-bordered table-striped'>";
            echo "<thead>";
            if ($num_results > 0) {
//  $result->fetch_all(MYSQLI_ASSOC) returns a numeric array of all the Players retrieved with the query
                $players = $result->fetch_all(MYSQLI_ASSOC);
                echo "<table class='table table-bordered'><tr>";

//This dynamically retrieves header names
                foreach ($players[0] as $k => $v) {
                    echo "<th>" . $k . "</th>";
                }
                echo "</thead>";
                echo "<tbody>";
//Create a new row for each Player
                foreach ($players as $player) {
                    echo "<tr>";

                    foreach ($player as $k => $v) {

                        echo "<td>" . $v . "</td>";

                    }
                    echo "</tr>";
                }

                echo "</tbody>";
                echo "</table>";
            }
            $result->free();
            $mysqli->close();
        } else {
            echo "An error has occurred.  The item was not added. <a href='newPlayer.php'>Try again?</a>";
        }

    }
    ?>
</div>
</body>
</html>
