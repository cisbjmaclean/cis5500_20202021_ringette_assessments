<?php

session_start();
// Dylan Corriveau
// Date: December 6th, 2020
// CIS2225
//Final Exam 1

// extract the GET variable isbn
if(isset($_GET['id'])) {

    //they have an isbn in the url
    $id = $_GET['id'];

    // connect to db
    //require("/FinalPart1/config.php");
    define('DB_SERVER', 'localhost');
    define('DB_USERNAME', 'web_only_user');
    define('DB_PASSWORD', 'web_secret_password');
    define('DB_NAME', 'ringette');

    /* Attempt to connect to MySQL database */
    $mysqli = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

    $id = $mysqli->real_escape_string($id);

    // get the data for just the Player we want to edit!
    $query = "SELECT * FROM playerskill WHERE playerskill.skillId = $id";
    $result = $mysqli->query($query);

    $num_results = $result->num_rows;

    if ($num_results == 0) {
        $message = "Player not found.";
    } else {
        $row = $result->fetch_assoc();
        $playerId= $row['playerId'];
        $skillTypeCode = $row['skillTypeCode'];
        $points = $row['points'];
        $time = $row['skillTime'];
        $comments = $row['comments'];
    }

    $result->free();
    $mysqli->close();
} else {
    //the id is not provided
    $message = "Sorry, no id provided.";
}
?>
<!doctype html>
<html>
<head>
    <title>ringette Application - Edit Player Entry</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    2288
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div id="container">

    <!-- <p><a href="newPlayer.php">Add a new Player</a> - <a href="inventory.php">View all Players</a></p>-->

    <h1>ringette Application - Edit Player Entry</h1>
    <?php
    // if message gets set above it means there is a problem and we don't have a Player with that id to edit or it isn't provided
    if (isset($message)) {
        echo $message;
    } else {
        // we have all we need so let's display the Player
        ?>

        <div class="newPlayer-form">
            <form action="updateResult.php" method="post">
                <fieldset  class="scheduler-border">
                    <legend  class="scheduler-border">ringette Application - Update Result</legend>

                    <!-- --><?php /*if (isset($_GET["empty"])) {

                      echo "You have not entered all the required details.<br />";

                  }
                  */?>
                    <div class="form-group">
                        <label for="playerId">Player ID: </label>
                        <input type="text" class="form-control" id="playerId" value='<?php echo $playerId ?>' placeholder="Enter Player ID" name="playerId">
                    </div>
                    <div class="form-group">
                        <label for="skillTypeCode">Skill Type Code: </label>
                        <input type="text" class="form-control" id="skillTypeCode" value='<?php echo $skillTypeCode ?>' placeholder="Enter Skill Type Code" name="skillTypeCode">
                    </div>
                    <div class="form-group">
                        <label for="points">Points: </label>
                        <input type="text" class="form-control" id="points" value='<?php echo $points ?>' placeholder="Enter Player Points" name="points">
                    </div>
                    <div class="form-group">
                        <label for="skillTime">Time: </label>
                        <input type="text" class="form-control" id="skillTime" value='<?php echo $time ?>' placeholder="Enter Player Time" name="skillTime">
                    </div>
                    <div class="form-group">
                        <label for="comments">Comments: </label>
                        <input type="text" class="form-control" id="comments" value='<?php echo $comments ?>' placeholder="Enter Comments" name="comments">
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control" id="id" value='<?php echo $id ?>'  name="id">
                        <button type="submit" name="submit" class="btn btn-primary btn-block">Update</button>
                    </div>
                </fieldset>
            </form>
        </div>
        <?php
    } // close the if no Player found $message above
    ?>
</body>
</html>
