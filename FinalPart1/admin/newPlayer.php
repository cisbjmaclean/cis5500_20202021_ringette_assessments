<?php
session_start();
// Dylan Corriveau
// Date: December 6th, 2020
// CIS2225
//Final Exam 1

?>
<!doctype html>
<html>
<head>
    <title>Add New Player Form</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    2288
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div>
    <div class="container">
        <form action="addPlayer.php" method="post">
            <h2>ringette Application - New Player Entry</h2>
            <?php
            if(!$_SESSION['loggedIn']){
                echo "Please log in!" . "<a href='/login.php' class ='btn btn-primary btn-block'>Login</a>";
            }else{
            $msg = "";

            if (isset($_GET["error"])) {

                if($_GET["error"] == 'empty') {

                    $msg = "You have not entered all the required details.";
                }else if($_GET["error"] == 'db') {

                    $msg = "DB error.Book not added.";
                }else if($_GET["error"] == 'noform') {

                    $msg = "You must fill out a new book form.";
                }

            }
            echo "<p class='error'>$msg</p>";
            ?>
            <div class="form-group">
                <label for="firstName">firstName:</label>
                <input type="text" class="form-control" id="firstName" placeholder="Enter player first name" name="firstName">
            </div>
            <div class="form-group">
                <label for="lastName">lastName:</label>
                <input type="text" class="form-control" id="lastName" placeholder="Enter player last name" name="lastName">
            </div>
            <div class="form-group">
                <label for="dob">DOB: </label>
                <input type="text" class="form-control" id="dob" placeholder="Enter player birth date" name="dob">
            </div>
                <div class="form-group">
                    <label for="active">Is player active:</label>
                    <select name="active" id="active">
                        <option value="1">yes</option>
                        <option value="0">no</option>
                    </select>
                </div>
            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-primary btn-block">Add Player</button>
            </div>
            <?php
            }
            ?>
        </form>
    </div>
</body>
</html>