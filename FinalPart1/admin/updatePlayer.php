<?php
session_start();
// Dylan Corriveau
// Date: December 6th, 2020
// CIS2225
//Final Exam 1
?>
<!doctype html>
<html>
<head>
    <title>ringette Application - Update</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    2288
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div id="container">

    <h1>ringette Application</h1>
    <!-- <p><a href="newPlayer.php">Add a new Player</a> - <a href="inventory.php">View all Players</a></p>-->
    <?php
    if(!$_SESSION['loggedIn']){
        echo "Please log in!" . "<a href='/FinalPart1/login.php' class ='btn btn-primary btn-block'>Log in</a>";
    }else {
        if (isset($_POST['submit'])) {
            // create short variable names
            $id = $_POST['id'];
            $firstName = $_POST['firstName'];
            $lastName = $_POST['lastName'];
            $dob = $_POST['dob'];
            $active = $_POST['active'];

            if (empty($firstName) || empty($lastName) || empty($dob) || empty($active)) {
                echo "You have not entered all the required details.<br />"
                    . "Please go back and try again.</body></html>";
                exit;
            }

            define('DB_SERVER', 'localhost');
            define('DB_USERNAME', 'web_only_user');
            define('DB_PASSWORD', 'web_secret_password');
            define('DB_NAME', 'ringette');

            /* Attempt to connect to MySQL database */
            $mysqli = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
            //$PlayerId=$mysqli->real_escape_string($PlayerId);
            $id = $mysqli->real_escape_string($id);
            $firstName = $mysqli->real_escape_string($firstName);
            $lastName = $mysqli->real_escape_string($lastName);
            $dob = $mysqli->real_escape_string($dob);
            $active = $mysqli->real_escape_string($active);

            // example UPDATE query
            $query = "UPDATE player SET firstName='$firstName', lastName='$lastName', dob='$dob', activeTypeCode=$active WHERE player.id=$id LIMIT 1";
            $result = $mysqli->query($query);

            if ($result) {
                echo $mysqli->affected_rows . " player updated in database. <a href='/FinalPart1/index.php'>View all Players</a>";
                //select Player
                //Order Detail Report Query
                $query = "SELECT *
             FROM `player`
                where player.id=$id";


// Here we use our $db object created above and run the query() method. We pass it our query from above.
                $result = $mysqli->query($query);

                // Here we 'get' the num_rows attribute of our $result object - this is key to knowing if we should show the results or
// display an error message, or perhaps just to say we don't have any results.
                $num_results = $result->num_rows;

                //echo "<p>Total Results: $num_results</p>";

                if ($num_results > 0) {
                    //  $result->fetch_all(MYSQLI_ASSOC) returns a numeric array of all the Players retrieved with the query
                    $players = $result->fetch_all(MYSQLI_ASSOC);

                    echo "<table class='table table-bordered'><tr>";
                    //This dynamically retieves header names
                    foreach ($players[0] as $k => $v) {
                        echo "<th>" . $k . "</th>";
                    }
                    echo "</tr>";
                    //Create a new row for each Player
                    foreach ($players as $player) {
                        echo "<tr>";
                        foreach ($player as $k => $v) {
                            echo "<td>" . $v . "</td>";
                        }
                        echo "</tr>";
                    }
                    echo "</table>";
                } else {
                    // if no results
                    echo "<p>Sorry there are no entries in the database.</p>";
                }
                $result->free();
                $mysqli->close();

            } else {
                echo "An error has occurred.  The item was not updated.";
            }
        } else {
            header("location:viewPlayers.php");
            exit();

        }
    }
    ?>
</div>
</body>
</html>