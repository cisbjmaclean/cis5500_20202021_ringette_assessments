<?php
// Dylan Corriveau
// Date: December 6th, 2020
// CIS2288
//Final Exam 1
session_start();

$id = "";
$msg="";
// Process delete operation after confirmation
if (isset($_GET["id"]) && !empty($_GET["id"])){
    //Create DB conection object
    define('DB_SERVER', 'localhost');
    define('DB_USERNAME', 'web_only_user');
    define('DB_PASSWORD', 'web_secret_password');
    define('DB_NAME', 'ringette');

    /* Attempt to connect to MySQL database */
    $mysqli = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    //Sanitize the parameter
    $id = $mysqli->real_escape_string($_GET['id']);
    // Prepare a delete statement
    if ($stmt = $mysqli->prepare("DELETE FROM playerskill WHERE playerskill.skillId = ?")) {
        // Bind variables to the prepared statement as parameters
        $stmt->bind_param("i", $id);

        //Set parameter and execute
        $id = $mysqli->real_escape_string($_GET["id"]);
        // Attempt to execute the prepared statement
        if ($stmt->execute()) {

            // Close statement
            $stmt->close();

            $msg= "Result ".$id." deleted";
            // Records deleted successfully. Redirect to landing page
            header("location: /FinalPart1/viewResults.php?msg=".$msg);
            exit();
        } else {
            echo "Oops! Something went wrong. Please try again later.";
        }
    }
// Close connection
    $mysqli->close();

} else{

    $msg = "Deletion Error" ;}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View Record</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div id="container">

    <p class="error"><?php echo $msg ?></p>

</div>
</body>
</html>
