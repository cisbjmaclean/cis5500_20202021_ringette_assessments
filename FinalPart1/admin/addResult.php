<?php
session_start();
// Dylan Corriveau
// Date: December 6th, 2020
// CIS2288
//Final Exam 1
?>
<!doctype html>
<html>
<head>
    <title>ringette Application Player - Add New Player</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="/FinalPart1/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div id="container">

    <h1>ringette Application Player - Add Player</h1>
    <?php
    //$i = $_POST['i'];

    if(!isset($_SESSION['loggedIn']) || !$_SESSION['loggedIn']){
        echo "Please log in!" . "<a href='/FinalPart1/login.php' class ='btn btn-primary btn-block'>Add Player</a>";
    }else{
        require_once("utilities.php");
        define('DB_SERVER', 'localhost');
        define('DB_USERNAME', 'web_only_user');
        define('DB_PASSWORD', 'web_secret_password');
        define('DB_NAME', 'ringette');
        $playerName = $_POST['playerName'];
        print_r($playerName);
        echo count($playerName);
        $skill = $_POST['skill'];
        $points = $_POST['points'];
        $time = $_POST['time'];
        $comments = $_POST['comments'];
        $mysqli = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
      //  echo $i;
        for($i = 0; $i < count($playerName); $i++){
        // create short variable names
            echo "Name".$playerName[$i];

//        if (empty($firstName) || empty($lastName) || empty($dob) || empty($active)) {
//
//            header("location:newPlayer.php?error=empty");
//            exit();
//
//        }
//        //Create DB object

//        /* Attempt to connect to MySQL database */


        /*$playerName = $mysqli->real_escape_string($playerName[$i]);
        $skill= $mysqli->real_escape_string($skill);
        $points = $mysqli->real_escape_string($points[$i]);
        $time = $mysqli->real_escape_string($time[$i]);
        $comments = $mysqli->real_escape_string($comments[$i]);*/


        if (mysqli_connect_errno()) {
            echo "Error: Could not connect to database.  Please try again later.";
            exit;
        }



            $query = "INSERT INTO playerSkill (skillId, playerName, skillTypeCode, points, skillTime, comments) VALUES
        ( 0, '$playerName[$i]', '$skill' , '$points[$i]' , '$time[$i]' , '$comments[$i]')";
            // echo $query;
            $result = $mysqli->query($query);

        };
        if ($result) {
            echo $mysqli->affected_rows . " player inserted into database. <a href='newPlayer.php'>Add another?</a>";

            //Display Player inventory
            $query = "SELECT * FROM playerskill";
// Here we use our $mysqli object created above and run the query() method. We pass it our query from above.
            $result = $mysqli->query($query);

            $num_results = $result->num_rows;

            echo "<p>Number of results found: " . $num_results . "</p>";

            echo "<h2>Ringette Result list</h2>";
            echo "<table class='table table-bordered table-striped'>";
            echo "<thead>";
            if ($num_results > 0) {
//  $result->fetch_all(MYSQLI_ASSOC) returns a numeric array of all the Players retrieved with the query
                $players = $result->fetch_all(MYSQLI_ASSOC);
                echo "<table class='table table-bordered'><tr>";

//This dynamically retrieves header names
                foreach ($players[0] as $k => $v) {
                    echo "<th>" . $k . "</th>";
                }
                echo "</thead>";
                echo "<tbody>";
//Create a new row for each Player
                foreach ($players as $player) {
                    echo "<tr>";

                    foreach ($player as $k => $v) {

                        echo "<td>" . $v . "</td>";

                    }
                    echo "</tr>";
                }

                echo "</tbody>";
                echo "</table>";
            }
            $result->free();
            $mysqli->close();
        } else {
            echo "An error has occurred.  The item was not added. <a href='newResult.php'>Try again?</a>";
        }

    }
    ?>
</div>
</body>
</html>
