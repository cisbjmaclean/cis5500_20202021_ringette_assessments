<?php
?>

<?php
session_start();
// Dylan Corriveau
// Date: December 6th, 2020
// CIS2225
//Final Exam 1
require("../config.php");
?>
<!doctype html>
<html>
<head>
    <title>Add New Result Form</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    2288
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
</head>
<body>
<div>
    <div class="container">
        <form action="newResult.php" method="post">
            <h2>ringette Application - Skill Select</h2>
            <label for="skill">Skill:</label>
            <select name="skill" id="skill">
                <option value="1">Agility</option>
                <option value="2">Butterfly</option>
                <option value="3">Shooting</option>
                <option value="4">Passing</option>
                <option value="5">Speed</option>
            </select>
            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-primary btn-block">Submit</button>
            </div>
</body>
</html>
