<?php

// Date: Nov 17, 2019
// Last Edit Author: jdkitson
// Purpose: Demonstrate how to search a mySQL database with PHP
session_start();


// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: ../../UserAccessPHP/UserAccess/login.php");
    exit;
}
if(!isset($_SESSION["userType"]) || $_SESSION["userType"] !== 2){
    header("location: ../../UserAccessPHP/UserAccess/welcome.php");
    exit;
}
require("../../Bootstrap/incPageHead.php");
?>


        <form action="add_codeType.php" method="post">
            <fieldset  class="scheduler-border">

<!--                <legend  class="scheduler-border">Book-O-Rama - New Book Entry</legend>-->
           <?php
            $msg = "";

            if (isset($_GET["error"])) {

                //if($_GET["error"] == 'empty') {

                  //  $msg = "You have not entered all the required details.";
                 if($_GET["error"] == 'db') {

                    $msg = "DB error.Book not added.";
                }else if($_GET["error"] == 'noform') {

                    $msg = "You must fill out a new book form.";
                }

              }
            echo "<p class='error'>$msg</p>";
              ?>
                <div class="form-group">
                    <label for="codeTypeId">codeTypeId:</label>
                    <input type="text" class="form-control" id="codeTypeId" placeholder="Enter codeTypeId" name="isbn">
                </div>
                <div class="form-group">
                    <label for="englishDescription">englishDescription:</label>
                    <input type="text" class="form-control" placeholder="Enter englishDescription" id="author" name="author">
                </div>
                <div class="form-group">
                    <label for="frenchDescription">frenchDescription:</label>
                    <input type="text" class="form-control" id="frenchDescription" placeholder="Enter frenchDescription" name="frenchDescription">
                </div>
                <div class="form-group">
                    <label for="createdDateTime">createdDateTime</label>
                    <input type="text" class="form-control" placeholder="Enter createdDateTime" id="price" name="price">
                </div>
                <div class="form-group">
                    <label for="createdUserId">createdUserId</label>
                    <input type="text" class="form-control" id="price" placeholder="Enter createdUserId" name="price">
                </div>
                <div class="form-group">
                    <label for="updatedDateTime">updatedDateTime</label>
                    <input type="text" class="form-control" id="price" placeholder="Enter updatedDateTime" name="price">
                </div>
                <div class="form-group">
                    <label for="updatedUserId">updatedUserId</label>
                    <input type="text" class="form-control" id="price" placeholder="Enter updatedUserId" name="price">
                </div>
                <div class="form-group">
                    <button type="submit" name="submit" class="btn btn-primary btn-block">Submit</button>
                </div>
            </fieldset>
        </form>
    </div>
</body>
</html>
