<?php
/*
 Date: March 8, 2021
 Author: Cameron MacDonald
 Purpose: Page to delete service from database
*/
session_start();

// Check if the user is logged in, if not then redirect him to login page
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: ../../UserAccessPHP/UserAccess/login.php");
    exit;
}
if(!isset($_SESSION["userType"]) || $_SESSION["userType"] !== 2){
    header("location: ../../UserAccessPHP/UserAccess/welcome.php");
    exit;
}
$pageTitle = "Service - Delete";
//include("incPageHead.php");

//This page will delete the selected
@ $db = new mysqli('localhost', 'root', '', 'curriculum');

if (mysqli_connect_errno()) {
    echo "Error: Could not connect to database.  Please try again later.</body></html>";
    exit;
}

//Logged in validation
//if (isset($_SESSION["valid"])) {

if (isset($_GET["codeTypeId"])) {
    $codeTypeId = $_GET["codeTypeId"];
    $codeValueId = $_GET['codeValueId'];

    //Real escape string
    $codeTypeId = $db->real_escape_string($codeTypeId);
    $codeValueId = $db->real_escape_string($codeValueId);

    //setting query
    $query = "DELETE FROM codevalue WHERE codeTypeId = '$codeTypeId' AND codeValueSequence= '$codeValueId'";

    // Attempt delete query execution
    if(mysqli_query($db, $query)){
        $message = "<div class='alert alert-success'>Delete Success <a href='codeType.php'>View All codetypes</a></div>";
        echo $message;
        exit;
    } else{
        $message = "<div class='alert alert-danger'>There was a problem with your query $query. " . mysqli_error($db) . " <a href=\"javascript:history.back()\">Go Back</a></div>";
        echo $message;
        exit;
    }
}

//}
//else {
//    echo "<h3>You are not logged in.</h3>";
echo "<a href='codeValue.php'>Click to go home!</a>";
//}
$db->close();
//include("incPageFoot.php");
?>
