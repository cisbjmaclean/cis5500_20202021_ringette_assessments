<?php
session_start();
// Dylan Corriveau
// Date: December 6th, 2020
// CIS2225
//Final Exam 1

?>
<!doctype html>
<html>
<head>
    <title>Add New Player Form</title>
    <link href="/Ringette/css/formStyles.css" rel="stylesheet" />
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
</head>
<body>
<div>
    <div class="container">
        <ul>
            <li><a href="/Ringette/home.php">Home</a></li>
            <li><a href="/Ringette/players.php">Players</a></li>
            <li><a href="/Ringette/skills.php">Skills</a></li>
            <li><a href='/Ringette/logout.php' style="margin-left: 725px; margin-top: 7px"  title='Log Out' class='btn btn-info' data-toggle='tooltip'>Logout</a></li>
        </ul>
        <form action="addPlayer.php" method="post">
            <h2>Ringette PEI - New Player Entry Form</h2>
            <?php
            if(!isset($_SESSION["userType"]) && $_SESSION["userType"] == 2){
                echo "Please log in!" . "<a href='/Ringette/users/login.php' class ='btn btn-primary btn-block'>Login</a>";
            }else{
            $msg = "";

            if (isset($_GET["error"])) {

                if($_GET["error"] == 'empty') {

                    $msg = "You have not entered all the required details.";
                }else if($_GET["error"] == 'db') {

                    $msg = "DB error.Book not added.";
                }else if($_GET["error"] == 'noform') {

                    $msg = "You must fill out a new book form.";
                }

            }
            echo "<p class='error'>$msg</p>";
            ?>
            <div class="form-group">
                <label for="firstName">First Name:</label>
                <input type="text" class="form-control" id="firstName" placeholder="Enter player first name" name="firstName">
            </div>
            <div class="form-group">
                <label for="lastName">Last Name:</label>
                <input type="text" class="form-control" id="lastName" placeholder="Enter player last name" name="lastName">
            </div>
            <div class="form-group">
                <label for="dob">DOB: </label>
                <input type="text" class="form-control" id="dob" placeholder="Enter player birth date" name="dob">
            </div>
                <div class="form-group">
                    <label for="active">Is player active:</label>
                    <select name="active" id="active">
                        <option value="1">yes</option>
                        <option value="0">no</option>
                    </select>
                </div>
            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-primary btn-block">Add Player</button>
            </div>
            <?php
            }
            ?>
        </form>
    </div>
</body>
</html>