<?php
session_start();
// Dylan Corriveau
// Date: December 6th, 2020
// CIS2225
//Final Exam 1
?>
<!doctype html>
<html>
<head>
    <title>Ringette Application - Update</title>
    <meta charset="utf-8">
    <link href="/Ringette/css/formStyles.css" rel="stylesheet" />
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
</head>
<body>
<div id="container">
    <ul>
        <li><a href="/Ringette/home.php">Home</a></li>
        <li><a href="/Ringette/players.php">Players</a></li>
        <li><a href="/Ringette/skills.php">Skills</a></li>
        <li><a href='/Ringette/logout.php' style="margin-left: 1625px; margin-top: 7px"  title='Log Out' class='btn btn-info' data-toggle='tooltip'>Logout</a></li>
    </ul>

    <h1>Ringette Application</h1>
    <?php
    if(!isset($_SESSION["userType"]) && $_SESSION["userType"] == 2){
        echo "Please log in!" . "<a href='/Ringette/users/login.php' class ='btn btn-primary btn-block'>Log in</a>";
    }else {
        if (isset($_POST['submit'])) {
            // create short variable names
            $id = $_POST['id'];
            $firstName = $_POST['firstName'];
            $lastName = $_POST['lastName'];
            $dob = $_POST['dob'];
            $active = $_POST['active'];

            if (empty($firstName) || empty($lastName) || empty($dob)) {
                echo "You have not entered all the required details.<br />"
                    . "Please go back and try again.</body></html>";
                exit;
            }

            define('DB_SERVER', 'localhost');
            define('DB_USERNAME', 'web_only_user');
            define('DB_PASSWORD', 'web_secret_password');
            define('DB_NAME', 'ringette');

            $mysqli = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
            $id = $mysqli->real_escape_string($id);
            $firstName = $mysqli->real_escape_string($firstName);
            $lastName = $mysqli->real_escape_string($lastName);
            $dob = $mysqli->real_escape_string($dob);
            $active = $mysqli->real_escape_string($active);
            $query = "UPDATE player SET firstName='$firstName', lastName='$lastName', dob='$dob', activeTypeCode=$active WHERE player.id=$id LIMIT 1";
            $result = $mysqli->query($query);
            if ($result) {
                echo $mysqli->affected_rows . " player updated in database. <a href='/Ringette/players.php'>View all Players</a>";
                $query = "SELECT *
             FROM `player`
                where player.id=$id";
                $result = $mysqli->query($query);
                $num_results = $result->num_rows;
                if ($num_results > 0) {
                    //  $result->fetch_all(MYSQLI_ASSOC) returns a numeric array of all the Players retrieved with the query
                    $players = $result->fetch_all(MYSQLI_ASSOC);

                    echo "<table class='table table-bordered'><tr>";
                    //This dynamically retieves header names
                    foreach ($players[0] as $k => $v) {
                        echo "<th>" . $k . "</th>";
                    }
                    echo "</tr>";
                    //Create a new row for each Player
                    foreach ($players as $player) {
                        echo "<tr>";
                        foreach ($player as $k => $v) {
                            echo "<td>" . $v . "</td>";
                        }
                        echo "</tr>";
                    }
                    echo "</table>";
                } else {
                    echo "<p>Sorry there are no entries in the database.</p>";
                }
                $result->free();
                $mysqli->close();
            } else {
                echo "An error has occurred.  The item was not updated.";
            }
        } else {
            header("location:players.php");
            exit();
        }
    }
    ?>
</div>
</body>
</html>