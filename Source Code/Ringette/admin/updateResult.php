<?php
session_start();
// Dylan Corriveau
// Date: December 6th, 2020
// CIS2225
//Final Exam 1
?>
<!doctype html>
<html>
<head>
    <title>R ingette Application - Update</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="assets/img/ringette.jpg" />
    <link href="/Ringette/css/formStyles.css" rel="stylesheet" />
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
</head>
<body>
<div id="container">
    <ul>
        <li><a href="/Ringette/home.php">Home</a></li>
        <li><a href="/Ringette/players.php">Players</a></li>
        <li><a href="/Ringette/skills.php">Skills</a></li>
        <li><a href='/Ringette/logout.php' style="margin-left: 1825px; margin-top: 7px"  title='Log Out' class='btn btn-info' data-toggle='tooltip'>Logout</a></li>
    </ul>
    <h1>Ringette Application - Update Skills</h1>
    <?php
    //if(!$_SESSION['loggedIn']){
    if(@!isset($_SESSION["userType"]) && $_SESSION["userType"] == 2){
        echo "Please log in!" . "<a href='/Ringette/login.php' class ='btn btn-primary btn-block'>Log in</a>";
    }else {
        if (isset($_POST['submit'])) {
            // create short variable names
            $id = $_POST['id'];
            $playerName = $_POST['playerName'];
            $skillTypeCode = $_POST['skillTypeCode'];
            $points = $_POST['points'];
            $time = $_POST['skillTime'];
            $comments = $_POST['comments'];

            define('DB_SERVER', 'localhost');
            define('DB_USERNAME', 'web_only_user');
            define('DB_PASSWORD', 'web_secret_password');
            define('DB_NAME', 'ringette');

            $mysqli = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

            $id = $mysqli->real_escape_string($id);
            $playerName = $mysqli->real_escape_string($playerName);
            $skillTypeCode = $mysqli->real_escape_string($skillTypeCode);
            $points = $mysqli->real_escape_string($points);
            $time = $mysqli->real_escape_string($time);
            $comments = $mysqli->real_escape_string($comments);


            $query = "UPDATE playerskill SET playerName='$playerName', skillTypeCode='$skillTypeCode', points='$points', skillTime='$time' , comments='$comments' WHERE playerskill.skillId=$id LIMIT 1";
            $result = $mysqli->query($query);

            if ($result) {
                echo $mysqli->affected_rows . " Skill updated in database. <a href='/Ringette/home.php'>View all Results</a>";
                //select Player
                //Order Detail Report Query
                $query = "SELECT * FROM `playerSkill` where playerSkill.skillId=$id";

                $result = $mysqli->query($query);

                $num_results = $result->num_rows;


                if ($num_results > 0) {
                    $players = $result->fetch_all(MYSQLI_ASSOC);

                    echo "<table class='table table-bordered'><tr>";
                    foreach ($players[0] as $k => $v) {
                        echo "<th>" . $k . "</th>";
                    }
                    echo "</tr>";
                    foreach ($players as $player) {
                        echo "<tr>";
                        foreach ($player as $k => $v) {
                            echo "<td>" . $v . "</td>";
                        }
                        echo "</tr>";
                    }
                    echo "</table>";
                } else {
                    echo "<p>Sorry there are no entries in the database.</p>";
                }
                $result->free();
                $mysqli->close();

            } else {
                echo "An error has occurred.  The item was not updated.";
            }
        } else {
            header("location:skills.php");
            exit();

        }
    }
    ?>
</div>
</body>
</html>