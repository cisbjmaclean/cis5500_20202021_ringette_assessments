<?php
// Dylan Corriveau
// Date: December 6th, 2020
// CIS2288
//Final Exam 1
session_start();

$id = "";
$msg="";
// Process delete operation after confirmation
if (isset($_GET["id"]) && !empty($_GET["id"])){
    //Create DB conection object
    define('DB_SERVER', 'localhost');
    define('DB_USERNAME', 'web_only_user');
    define('DB_PASSWORD', 'web_secret_password');
    define('DB_NAME', 'ringette');

    /* Attempt to connect to MySQL database */
    $mysqli = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    //Sanitize the parameter
    $id = $mysqli->real_escape_string($_GET['id']);
    // Prepare a delete statement
    if ($stmt = $mysqli->prepare("DELETE FROM player WHERE player.id = ?")) {
        // Bind variables to the prepared statement as parameters
        $stmt->bind_param("i", $id);

        //Set parameter and execute
        $id = $mysqli->real_escape_string($_GET["id"]);
        // Attempt to execute the prepared statement
        if ($stmt->execute()) {

            // Close statement
            $stmt->close();

            $msg= "Player ".$id." deleted";
            // Records deleted successfully. Redirect to landing page
            header("location: /Ringette/players.php?msg=".$msg);
            exit();
        } else {
            echo "Oops! Something went wrong. Please try again later.";
        }
    }
// Close connection
    $mysqli->close();

} else{

    $msg = "Deletion Error" ;}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View Record</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="assets/img/ringette.jpg" />
    <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
    <link href="/cis5500_bootstrap_template/css/styles.css" rel="stylesheet" />
</head>
<body>
<div id="container">

    <p class="error"><?php echo $msg ?></p>

</div>
</body>
</html>
