<?php
session_start();
// Dylan Corriveau
// Date: December 6th, 2020
// CIS2225
//Final Exam 1
require("../users/config.php");

@$skill = $_POST['skill'];
$query = "SELECT concat(player.firstName, ' ', player.lastName) FROM player";
$result = $mysqli->query($query);
//  echo $skill;
$i = 0;
$num_results = $result->num_rows;
$players = $result->fetch_all(MYSQLI_ASSOC);
?>
<!doctype html>
<html>
<head>
    <title>Add New Result Form</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/Ringette/css/formStyles.css" rel="stylesheet" />
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <style>
        table{
            width: 100%;
            margin: 20px 0;
            border-collapse: collapse;
        }
        table, th, td{
            border: 1px solid #cdcdcd;
        }
        table th, table td{
            padding: 5px;
            text-align: left;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script>
        $(document).ready(function(){
                //incrementer
                let i = 1;
                $(".add-row").on("click",function(){

                    let markup = "<tr><td><select name='playerName[]'>"+
                        "<?php
                            foreach ($players as $player) {
                                $p = 0;
                                foreach ($player as $k => $v) {

                                    if ($k == 'skillId') {
                                        $id = $v;
                                    } else {
                                        echo "<option value='$v'>  $v  </option>";
                                    }
                                    $p++;
                                }
                            }
                            ?>" + "</select>"+
                        "<?php
                            if($skill == 1 || $skill == 5 ) {

                            echo "<td><input type='text' class='form-control' placeholder='Enter Player Time' name='time[]'> </td>";
                            echo "<input type='hidden' class='form-control' value='0' placeholder='Enter Player Points' name='points[]'>"; ?>"
                        +"<?php }else{ echo "<td><input type='text' class='form-control' placeholder='Enter Player Points' name='points[]'></td>";
                            echo "<input type='hidden' class='form-control' value='0' placeholder='Enter Player Time' name='time[]'>";} ?>"
                        +"<td><input type='text' placeholder='Enter comments' name='comments[" +i+ "]'></td>" +
                        "<td><input type='checkbox' name='record'></td></tr>";
                    $("table tbody").append(markup);
                    i++;
                    i = "<?php $i; ?>";
                });
                // Find and remove selected table rows
                $(".delete-row").on("click",function(){
                    //new incrementer
                    let a = 0;
                    $("table tbody").find('input[name="record"]').each(function(){
                        if($(this).is(":checked")){
                            a++;
                            $(this).parents("tr").remove();
                        }
                    });
                    i-=a;
                });
            }
        );
    </script>
</head>
<body>
<ul>
    <li><a href="/Ringette/home.php">Home</a></li>
    <li><a href="/Ringette/players.php">Players</a></li>
    <li><a href="/Ringette/skills.php">Skills</a></li>
    <li><a href="/Ringette/users/account.php">Account</a></li>
    <li><a href='/Ringette/logout.php' style="margin-left: 1725px; margin-top: 7px"  title='Log Out' class='btn btn-info' data-toggle='tooltip'>Logout</a></li>
</ul>
<div>
    <div class="container">
        <form action="addResult.php" method="post">
            <h2>ringette Application - New Player Entry</h2>
            <?php
            if(@!isset($_SESSION["userType"]) && $_SESSION["userType"] == 2){
            //if(!$_SESSION['loggedIn']){
                echo "Please log in!" . "<a href='/Ringette/users/login.php' class ='btn btn-primary btn-block'>Login</a>";
            }else {
                $msg = "";
                if (isset($_GET["error"])) {

                    if ($_GET["error"] == 'empty') {
                        $msg = "You have not entered all the required details.";
                    } else if ($_GET["error"] == 'db') {

                        $msg = "DB error.Result not added.";
                    } else if ($_GET["error"] == 'noform') {
                        $msg = "You must fill out a new result form.";
                    }
                }
            }
            foreach ($players[0] as $k => $v) {
            }
            echo "<p class='error'>$msg</p>";
            ?>
            <table>
                <tr>
                    <td>
                        <select name="playerName[]" id="playerName[]">
                            <?php
                            foreach ($players as $player) {
                                $p = 0;
                                foreach ($player as $k => $v) {

                                    if ($k == 'skillId') {
                                        $id = $v;
                                    } else {
                                        echo "<option value='$v'>" . $v . "</option>";
                                    }
                                    $p++;
                                }
                            }
                            ?>
                        </select>
                    </td>
                    <td>
                        <?php
                        if($skill == 1 || $skill == 5 ) {
                            echo'<input type="text" class="form-control" id="time[]" placeholder="Enter Player Time" name="time[]">
                                </td>
                                ';
                            echo '<input type="hidden" class="form-control" id="points[]" value="0" placeholder="Enter Player Points" name="points[]">
                                
                                ';
                        }else{
                            echo'<input type="hidden" class="form-control" id="time[]" value="0" placeholder="Enter Player Time" name="time[]">
                                
                                <td>';
                            echo '<input type="text" class="form-control" id="points[]" placeholder="Enter Player Points" name="points[]">
                                </td>
                                '; }
                        ?>
                    <td>
                        <input type="text" class="form-control" id="comments[]" placeholder="Enter Comments" name="comments[]">
                    </td>
                    <td>
                        <input type="checkbox" class="form-control" name="record">
                    </td>
                </tr>
                <input type="hidden" class="form-control" id="skill" value='<?php echo $skill ?>'  name="skill">
                <input type="hidden" class="form-control" id="i" value='<?php echo $i ?>'  name="i">
            </table>
            <div class="form-group">
                <button type="button" class="add-row">Add Row</button>
            </div>
            <div class="form-group">
                <button type="button" class="delete-row">Delete Row(s)</button>
            </div>
            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-primary btn-block">Add Results</button>
            </div>
        </form>
    </div>
</body>
</html>