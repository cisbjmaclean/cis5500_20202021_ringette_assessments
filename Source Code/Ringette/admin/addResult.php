<?php
session_start();
// Dylan Corriveau
// Date: December 6th, 2020
// CIS2288
//Final Exam 1
?>
<!doctype html>
<html>
<head>
    <title>Ringette Application Player - Add New Player</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="assets/img/ringette.jpg" />
    <link href="/Ringette/css/formStyles.css" rel="stylesheet" />
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
</head>
<body>
<div id="container">
    <ul>
        <li><a href="/Ringette/home.php">Home</a></li>
        <li><a href="/Ringette/players.php">Players</a></li>
        <li><a href="/Ringette/skills.php">Skills</a></li>
        <li><a href='/Ringette/logout.php' style="margin-left: 1825px; margin-top: 7px"  title='Log Out' class='btn btn-info' data-toggle='tooltip'>Logout</a></li>
    </ul>
    <h1>Ringette Application Player - Add Skills Result</h1>
    <?php
    //$i = $_POST['i'];

    //if(!isset($_SESSION['loggedIn']) || !$_SESSION['loggedIn']){
    if(@!isset($_SESSION["userType"]) && $_SESSION["userType"] == 2){
        echo "Please log in!" . "<a href='../users/login.php' class ='btn btn-primary btn-block'>Add Player</a>";
    }else{
        require_once("utilities.php");
        define('DB_SERVER', 'localhost');
        define('DB_USERNAME', 'web_only_user');
        define('DB_PASSWORD', 'web_secret_password');
        define('DB_NAME', 'ringette');
        $playerName = $_POST['playerName'];
        //print_r($playerName);
        //echo count($playerName);
        $skill = $_POST['skill'];
        $points = $_POST['points'];
        $time = $_POST['time'];
        $comments = $_POST['comments'];
        $mysqli = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
        //  echo $i;
        for($i = 0; $i < count($playerName); $i++){
            // create short variable names
            //echo "Name".$playerName[$i];
            if (mysqli_connect_errno()) {
                echo "Error: Could not connect to database.  Please try again later.";
                exit;
            }
            $query = "INSERT INTO playerSkill (skillId, playerName, skillTypeCode, points, skillTime, comments) VALUES
        ( 0, '$playerName[$i]', '$skill' , '$points[$i]' , '$time[$i]' , '$comments[$i]')";
            // echo $query;
            $result = $mysqli->query($query);

        };
        if ($result) {
            echo $mysqli->affected_rows . " player inserted into database. <a href='newPlayer.php'>Add another?</a>";

            //Display Player inventory
            $query = "SELECT * FROM playerskill";
// Here we use our $mysqli object created above and run the query() method. We pass it our query from above.
            $result = $mysqli->query($query);

            $num_results = $result->num_rows;

            echo "<p>Number of results found: " . $num_results . "</p>";

            echo "<h2>Ringette Result list</h2>";
            echo "<table class='table table-bordered table-striped'>";
            echo "<thead>";
            if ($num_results > 0) {
//  $result->fetch_all(MYSQLI_ASSOC) returns a numeric array of all the Players retrieved with the query
                $players = $result->fetch_all(MYSQLI_ASSOC);
                echo "<table class='table table-bordered'><tr>";

//This dynamically retrieves header names
                foreach ($players[0] as $k => $v) {
                    echo "<th>" . $k . "</th>";
                }
                echo "</thead>";
                echo "<tbody>";
//Create a new row for each Player
                foreach ($players as $player) {
                    echo "<tr>";

                    foreach ($player as $k => $v) {

                        echo "<td>" . $v . "</td>";

                    }
                    echo "</tr>";
                }

                echo "</tbody>";
                echo "</table>";
            }
            $result->free();
            $mysqli->close();
        } else {
            echo "An error has occurred.  The item was not added. <a href='newResult.php'>Try again?</a>";
        }

    }
    ?>
</div>
</body>
</html>
