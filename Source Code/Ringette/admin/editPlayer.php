<?php

session_start();
// Dylan Corriveau
// Date: December 6th, 2020
// CIS2225
//Final Exam 1

// extract the GET variable isbn
if(isset($_GET['id'])) {

    //they have an isbn in the url
    $id = $_GET['id'];

    // connect to db
    //require("/FinalPart1/config.php");
    define('DB_SERVER', 'localhost');
    define('DB_USERNAME', 'web_only_user');
    define('DB_PASSWORD', 'web_secret_password');
    define('DB_NAME', 'ringette');

    /* Attempt to connect to MySQL database */
    $mysqli = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

    $id = $mysqli->real_escape_string($id);

    // get the data for just the Player we want to edit!
    $query = "SELECT * FROM player WHERE player.id = $id";
    $result = $mysqli->query($query);

    $num_results = $result->num_rows;

    if ($num_results == 0) {
        $message = "Player not found.";
    } else {
        $row = $result->fetch_assoc();
        $firstName = $row['firstName'];
        $lastName = $row['lastName'];
        $dob = $row['dob'];
        $active = $row['activeTypeCode'];
    }

    $result->free();
    $mysqli->close();
} else {
    //the id is not provided
    $message = "Sorry, no id provided.";
}
?>
<!doctype html>
<html>
<head>
    <title>Ringette Application - Edit Player Entry</title>
    <meta charset="utf-8">
    <link href="/Ringette/css/formStyles.css" rel="stylesheet" />
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
</head>
<body>
<div id="container">
    <ul>
        <li><a href="/Ringette/home.php">Home</a></li>
        <li><a href="/Ringette/players.php">Players</a></li>
        <li><a href="/Ringette/skills.php">Skills</a></li>
        <li><a href="/Ringette/users/account.php">Account</a></li>
        <li><a href='/Ringette/logout.php' style="margin-left:1725px; margin-top: 7px"  title='Log Out' class='btn btn-info' data-toggle='tooltip'>Logout</a></li>
    </ul>
    <!-- <p><a href="newPlayer.php">Add a new Player</a> - <a href="inventory.php">View all Players</a></p>-->
    <?php
    // if message gets set above it means there is a problem and we don't have a Player with that id to edit or it isn't provided
    if (isset($message)) {
        echo $message;
    } else {
        // we have all we need so let's display the Player
        ?>

        <div class="newPlayer-form">
            <form action="updatePlayer.php" method="post">
                <fieldset  class="scheduler-border">
                    <H1>Ringette Application - Update Player</H1>

                    <!-- --><?php /*if (isset($_GET["empty"])) {

                      echo "You have not entered all the required details.<br />";

                  }
                  */?>
                    <div class="form-group">
                        <label for="author">First Name: </label>
                        <input type="text" class="form-control" id="firstName" value='<?php echo $firstName ?>' placeholder="Enter Player First Name" name="firstName">
                    </div>
                    <div class="form-group">
                        <label for="title">Last Name: </label>
                        <input type="text" class="form-control" id="lastName" value='<?php echo $lastName ?>' placeholder="Enter Player Last Name" name="lastName">
                    </div>
                    <div class="form-group">
                        <label for="price">DOB: </label>
                        <input type="text" class="form-control" id="dob" value='<?php echo $dob ?>' placeholder="Enter Player Birth Date" name="dob">
                    </div>
                    <div class="form-group">
                        <label for="active">Is player active:</label>
                        <select name="active" id="active">
                            <option value="1">yes</option>
                            <option value="0">no</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control" id="id" value='<?php echo $id ?>'  name="id">
                        <button type="submit" name="submit" class="btn btn-primary btn-block">Update</button>
                    </div>
                </fieldset>
            </form>
        </div>
        <?php
    } // close the if no Player found $message above
    ?>
</body>
</html>
