<?php

session_start();
// Dylan Corriveau
// Date: December 6th, 2020
// CIS2225
//Final Exam 1

if(isset($_GET['id'])) {
    $id = $_GET['id'];
    define('DB_SERVER', 'localhost');
    define('DB_USERNAME', 'web_only_user');
    define('DB_PASSWORD', 'web_secret_password');
    define('DB_NAME', 'ringette');

    $mysqli = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

    $id = $mysqli->real_escape_string($id);

    $query = "SELECT * FROM playerskill WHERE playerskill.skillId = $id";
    $result = $mysqli->query($query);

    $num_results = $result->num_rows;

    if ($num_results == 0) {
        $message = "Player not found.";
    } else {
        $row = $result->fetch_assoc();
        $playerName= $row['playerName'];
        $skillTypeCode = $row['skillTypeCode'];
        $points = $row['points'];
        $time = $row['skillTime'];
        $comments = $row['comments'];
    }
    $result->free();
    $mysqli->close();
} else {
    $message = "Sorry, no id provided.";
}
?>
<!doctype html>
<html>
<head>
    <title>Ringette Application - Edit Skills Entry</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/Ringette/css/formStyles.css" rel="stylesheet" />
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

</head>
<body>
<div id="container">
    <ul>
        <li><a href="/Ringette/home.php">Home</a></li>
        <li><a href="/Ringette/players.php">Players</a></li>
        <li><a href="/Ringette/skills.php">Skills</a></li>
        <li><a href='/Ringette/logout.php' style="margin-left: 1625px; margin-top: 7px"  title='Log Out' class='btn btn-info' data-toggle='tooltip'>Logout</a></li>
    </ul>

    <!-- <p><a href="newPlayer.php">Add a new Player</a> - <a href="inventory.php">View all Players</a></p>-->

    <h1>Ringette Application - Edit Skills Entry</h1>
    <?php
    // if message gets set above it means there is a problem and we don't have a Player with that id to edit or it isn't provided
    if (isset($message)) {
        echo $message;
    } else {
        // we have all we need so let's display the Player
        ?>

        <div class="newPlayer-form">
            <form action="updateResult.php" method="post">
                <fieldset  class="scheduler-border">
                    <?php ?>
                    <div class="form-group">
                        <label for="playerName">Player Name: </label>
                        <input type="text" class="form-control" id="playerId" value='<?php echo $playerName ?>' placeholder="Enter Player Name" name="playerName">
                    </div>
                    <div class="form-group">
                        <label for="skillTypeCode">Skill Type Code: </label>
                        <input type="text" class="form-control" id="skillTypeCode" value='<?php echo $skillTypeCode ?>' placeholder="Enter Skill Type Code" name="skillTypeCode">
                    </div>
                    <div class="form-group">
                        <label for="points">Points: </label>
                        <input type="text" class="form-control" id="points" value='<?php echo $points ?>' placeholder="Enter Player Points" name="points">
                    </div>
                    <div class="form-group">
                        <label for="time">Time: </label>
                        <input type="text" class="form-control" id="skillTime" value='<?php echo $time ?>' placeholder="Enter Player Time" name="skillTime">
                    </div>
                    <div class="form-group">
                        <label for="comments">Comments: </label>
                        <input type="text" class="form-control" id="comments" value='<?php echo $comments ?>' placeholder="Enter Comments" name="comments">
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control" id="id" value='<?php echo $id ?>'  name="id">
                        <button type="submit" name="submit" class="btn btn-primary btn-block">Update</button>
                    </div>
                </fieldset>
            </form>
        </div>
        <?php
    }
    ?>
</body>
</html>
