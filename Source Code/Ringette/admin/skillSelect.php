<?php
?>

<?php
session_start();
// Dylan Corriveau
// Date: December 6th, 2020
// CIS2225
//Final Exam 1
require("../users/config.php");
?>
<!doctype html>
<html>
<head>
    <title>New Ringette Player Score</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/Ringette/css/formStyles.css" rel="stylesheet" />
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
</head>
<body>
<ul>
    <li><a href="/Ringette/home.php">Home</a></li>
    <li><a href="/Ringette/players.php">Players</a></li>
    <li><a href="/Ringette/skills.php">Skills</a></li>
    <li><a href="/Ringette/users/account.php">Account</a></li>
    <li><a href='/Ringette/logout.php' style="margin-left: 1725px; margin-top: 7px"  title='Log Out' class='btn btn-info' data-toggle='tooltip'>Logout</a></li>
</ul>
<div>
    <div class="container">
        <form action="newResult.php" method="post">
            <h2>Ringette Application - Skill Select</h2>
            <label for="skill">Skill:</label>
            <select name="skill" id="skill">
                <option value="1">Agility</option>
                <option value="2">Butterfly</option>
                <option value="3">Shooting</option>
                <option value="4">Passing</option>
                <option value="5">Speed</option>
            </select>
            <div class="form-group">
                <button type="submit" style="width: 100px" name="submit" class="btn btn-primary btn-block">Submit</button>
            </div>
</body>
</html>
