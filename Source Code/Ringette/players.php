<?php
session_start();
// Dylan Corriveau
// Date: December 6th, 2020
// CIS2288
//Final Exam 1

?>
<!doctype html>
<html lang="en">
<head>
    <title>Ringette Application - Players</title>
    <link href="css/formStyles.css" rel="stylesheet" />
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
</head>
<body>
<div id="container">
    <ul>
        <li><a href="home.php">Home</a></li>
        <li><a href="players.php">Players</a></li>
        <li><a href="skills.php">Skills</a></li>
        <li><a href='/Ringette/users/logout.php' style="margin-left: 1825px; margin-top: 7px"  title='Log Out' class='btn btn-info' data-toggle='tooltip'>Logout</a></li>
    </ul>
    <?php
    // set up connection
    require("config.php");
    //if(@!isset($_SESSION["userType"]) && $_SESSION["userType"] == 2){
    if(@!isset($_SESSION["userType"]) && $_SESSION["userType"] == 2){
        echo "Please log in!" . "<a href='/Ringette/users/login.php' class ='btn btn-primary btn-block'>Login</a>";

    }else{
        echo '<style> .loggedInCorner{display: block} .loggedInColumn{display: block}</style>';
    }


    //Display book inventory
    $query = "SELECT * FROM player";

    // Here we use our $db object created above and run the query() method. We pass it our query from above.
    $result = $mysqli->query($query);

    $num_results = $result->num_rows;
    if(isset($_GET['msg'])) {
        echo "<p>{$_GET['msg']}</p>";
    }
    echo "<h2>Ringette Application</h2>";
    echo "<p>Number of Players found: " . $num_results . "</p>";
    echo "<table class='table table-bordered table-striped'>";
    echo "<thead>";
    if ($num_results > 0) {
//  $result->fetch_all(MYSQLI_ASSOC) returns a numeric array of all the books retrieved with the query
        $players = $result->fetch_all(MYSQLI_ASSOC);
        echo "<table class='table table-bordered'><tr>";
//This dynamically retieves header names
        foreach ($players[0] as $k => $v) {

            echo "<th>" . $k . "</th>";

        }

        echo "<th>Action</th>";

        echo "</tr></thead>";
        echo "<tbody>";
//Create a new row for each book
        foreach ($players as $player) {
            echo "<tr>";
            $i = 0;

            foreach ($player as $k => $v) {

                if ($k == 'id') {
                    echo "<td>" . $v . "</td>";
                    $id = $v;
                } else {
                    echo "<td>" . $v . "</td>";
                }
                if (($i == count($player) - 1)) {
                    echo "<td class='loggedInColumn'>";
                    echo "<div class='btn-toolbar'>";
                    echo "<a href='admin/editPlayer.php?id=" . $id . "' title='Edit Record' class='btn btn-info btn-xs' data-toggle='tooltip'>Edit</a>";
                    echo "<a href='admin/deletePlayer.php?id=" . $id . "' title='Delete Record' class='btn btn-info btn-xs' data-toggle='tooltip'>Delete</a>";
                    echo "</div>";
                    echo "</td>";
                }
                $i++;
            }
            echo "</tr>";

        }

        echo "<tr><td colspan='6'>";
        echo "<a href='admin/addPlayer.php' title='View Record' class='btn btn-info' data-toggle='tooltip'>Add a New Player</a>";
        echo "</td></tr>";

        echo "</tbody>";
        echo "</table>";
    }
    // free result and disconnect
    $result->free();
    $mysqli->close();

    ?>
</div>
</body>
</html>
