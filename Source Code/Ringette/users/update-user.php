<?php
//require("../../Bootstrap/incPageHead.php");
// Initialize the session
session_start();


// Check if the user is logged in, if not then redirect to login page
if(!isset($_SESSION["userType"]) && $_SESSION["userType"] == 2){
//if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "config.php";

// Define variables and initialize with empty values
$new_name = "";
$new_password_err = $confirm_password_err = "";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){

    // Validate new password
    if(empty(trim($_POST["new_name"]))){
        $new_password_err = "Please enter the new name.";
    }  else{
        $new_name = trim($_POST["new_name"]);
    }
    $userType = trim($_POST['userType']);



    // Check input errors before updating the database
    if(empty($new_password_err)){
        // Prepare an update statement
        // Changed SQL parameters
        $sql = "UPDATE useraccess SET name = ? WHERE userAccessId = ?";

        if($stmt = $mysqli->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bind_param("si", $param_name, $param_id);

            // Set parameters
            $param_name = $new_name;
            $param_id = $_SESSION["id"];


            // Attempt to execute the prepared statement
            if($stmt->execute()){
                // Password updated successfully. Destroy the session, and redirect to login page
                header("location: /Ringette/home.php");
                exit();
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            $stmt->close();
        }
    }

    // Close connection
    $mysqli->close();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Updat User</title>
    <link href="/Ringette/css/formStyles.css" rel="stylesheet" />
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <ul>
        <li><a href="/Ringette/home.php">Home</a></li>
        <li><a href="/Ringette/players.php">Players</a></li>
        <li><a href="/Ringette/skills.php">Skills</a></li>
        <li><a href='/Ringette/logout.php' style="margin-left: 825px; margin-top: 7px"  title='Log Out' class='btn btn-info' data-toggle='tooltip'>Logout</a></li>
    </ul>

<div class="container">
    <fieldset>
    <h2>Update Information</h2>
    <p>Please fill out this form to change your information.</p>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <div class="form-group <?php echo (!empty($new_password_err)) ? 'has-error' : ''; ?>">
            <label>New Name</label>
            <input type="text" name="new_name" class="form-control" value="<?php echo $new_name; ?>">
            <span class="help-block"><?php echo $new_password_err; ?></span>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Submit">
            <a class="btn btn-link" href="/Ringette/home.php">Cancel</a>
        </div>
    </form>
    </fieldset>
</div>
